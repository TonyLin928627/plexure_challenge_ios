//
//  Consts.swift
//  Plexure_challenge_ios
//
//  Created by Tony Lin on 3/05/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import Foundation

let DATA_URL = "https://bitbucket.org/YahiaRagaePlex/plexure-challenge/raw/449a2452c03961d5d1a094af524148cc345523db/data.json"

let TOO_FAR_VAL:Double = 80*1000
