//
//  ItemCellTableViewCell.swift
//  Plexure_challenge_ios
//
//  Created by Tony Lin on 2/05/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import UIKit

class RestaurantCell: UITableViewCell {


    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var featuresLabel: UILabel!
    @IBOutlet weak var features: UIStackView!
    @IBOutlet weak var isFavoratied: UISwitch!
    
    var restaurantId: Int? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
