//
//  FavoritedRestaurantsTableViewController.swift
//  Plexure_challenge_ios
//
//  Created by Tony Lin on 2/05/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import UIKit

class FavoritedRestaurantsTableViewController: BaseTableViewController {
   
    
    override func populateData(){
        if BaseTableViewController.data == nil{
            
            showLoading()
            
            LoadData(
                
                completion: {data, errMsg in
                    
                    guard errMsg != nil else {
                        
                        
                        self.filteredData = data.filter({ (Restaurant) -> Bool in
                            return CheckFavoratedRestaurantId(id: Restaurant.id)
                        })
                        
                        
                        self.tableView.reloadData()
                        
                        self.refreshControl?.endRefreshing()
                        self.hideLoading()
                        
                        AllRestaurantsTableViewController.data = data
                        
                        return
                    }
                    
                    
                    DispatchQueue.main.async {
                        
                        self.hideLoading()
                        self.popErrDialog(errMsg: errMsg!)
                        
                    }
            })
        }
    }
    
    
    // MARK: - Table view data source
    
    @objc override func toggleFavorite(_ sender: UISwitch){
        
        let restaurantId = sender.tag
        debugPrint("set \(restaurantId) to \(sender.isOn)")
        
        if(sender.isOn){
            AddFavoratedRestaurantId(id: restaurantId)
        }else{
            RemoveFavoratedRestaurantId(id: restaurantId)

            guard let allData = AllRestaurantsTableViewController.data else{
                return
            }

            self.filteredData = allData.filter({ (Restaurant) -> Bool in
                return CheckFavoratedRestaurantId(id: Restaurant.id)
            })
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if BaseTableViewController.data != nil{
         
            self.filteredData = BaseTableViewController.data!.filter({ (Restaurant) -> Bool in
                return CheckFavoratedRestaurantId(id: Restaurant.id)
            })
            
            
            self.tableView.reloadData()
            
            self.refreshControl?.endRefreshing()
            self.hideLoading()
            
        }
        
    }
}
