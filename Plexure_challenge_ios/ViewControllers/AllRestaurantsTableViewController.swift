//
//  AllRestaurantsTableViewController.swift
//  Plexure_challenge_ios
//
//  Created by Tony Lin on 2/05/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import UIKit

class AllRestaurantsTableViewController: BaseTableViewController {
    
    override func populateData(){
        if BaseTableViewController.data == nil{
            
            showLoading()
            
            LoadData(
                
                completion: {data, errMsg in
                    
                    guard errMsg != nil else {
                        
                        self.filteredData = data
                        
                        
                        self.tableView.reloadData()
                        
                        self.refreshControl?.endRefreshing()
                        self.hideLoading()
                        
                        AllRestaurantsTableViewController.data = data
                        
                        self.setBadgeValue()
                        
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.hideLoading()
                        self.popErrDialog(errMsg: errMsg!)
                    }
                    
                    
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let allData = AllRestaurantsTableViewController.data else{
            return
        }
        
        self.filteredData = allData
        self.tableView.reloadData()
        self.setBadgeValue()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.tabBarItem.badgeValue = nil
    }
    
    private func setBadgeValue(){
        
        var badgeValue = 0
        if (AllRestaurantsTableViewController.data != nil){
            for restaurant in AllRestaurantsTableViewController.data! {
                if (CheckFavoratedRestaurantId(id: restaurant.id)){
                    badgeValue+=1
                }
            }
        }
        
        if badgeValue > 0 {
            self.tabBarItem.badgeValue = "\(badgeValue)"
        }else{
            self.tabBarItem.badgeValue = nil
        }
    }
    
    
    
    private var segmentIndex = 0
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let items = ["None" , "Deciding", "Assending"]
        let segments = UISegmentedControl(items : items)
        segments.selectedSegmentIndex = segmentIndex
        segments.addTarget(self, action: #selector(indexChanged(_:)), for: .valueChanged)
        
        let headerView = UIView()
        headerView.backgroundColor = .lightGray
        headerView.addSubview(segments)
        
        return headerView
    }
    
    @objc func indexChanged(_ sender: UISegmentedControl) {
        
        self.segmentIndex = sender.selectedSegmentIndex
        
        self.showLoading()
        let selectedIndex = sender.selectedSegmentIndex
        DispatchQueue.global().async{
            switch selectedIndex{
            case 0:
                print("None");
                self.filteredData = AllRestaurantsTableViewController.data!
                
                self.tableView.reloadData()
            case 1:
                print("Deciding")
                self.filteredData = (AllRestaurantsTableViewController.data!.sorted(by: { (Restaurant0
                    , Restaurant1) -> Bool in
                    return Restaurant0.distance > Restaurant1.distance
                }))
                
                self.tableView.reloadData()
            case 2:
                print("Assending")
                
                self.filteredData = (AllRestaurantsTableViewController.data!.sorted(by: { (Restaurant0
                    , Restaurant1) -> Bool in
                    return Restaurant0.distance <= Restaurant1.distance
                }))
                
            default:
                break
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
                self.hideLoading()
            }
        }
        
        
    }
    
    @objc override func toggleFavorite(_ sender: UISwitch){
        
        super.toggleFavorite(sender)
        setBadgeValue()
    }
}

