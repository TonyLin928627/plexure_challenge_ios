//
//  BaseTableViewController.swift
//  Plexure_challenge_ios
//
//  Created by Tony Lin on 3/05/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import UIKit
import SVProgressHUD

class BaseTableViewController: UITableViewController {

    static var data: [Restaurant]? = nil
    
    var filteredData: [Restaurant] = [Restaurant]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.refreshControl = refresh
        self.populateData()
        
    }
    
    @objc func refreshData() {
        AllRestaurantsTableViewController.data = nil
        self.populateData()
    }
    
    func populateData(){
        preconditionFailure("This method must be overridden")
    }
        
        
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredData.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! RestaurantCell
        
        let restaurant = self.filteredData[indexPath.row]
        let restaurantId = restaurant.id
        
        cell.restaurantId = restaurantId
        cell.name.text = restaurant.name
        cell.distance.text = "Distance: \(restaurant.distance/1000) KM"
        cell.address.text = "Loading address..." //"Address: \(restaurant.address)"
        cell.isFavoratied.isOn = CheckFavoratedRestaurantId(id: restaurant.id)
        
        cell.isFavoratied.tag = restaurant.id
        
        if (restaurant.distance > TOO_FAR_VAL){
            cell.isFavoratied.isUserInteractionEnabled = false
            cell.contentView.backgroundColor = .gray
        }else{
            cell.contentView.backgroundColor = .white
            cell.isFavoratied.isUserInteractionEnabled = true
            cell.isFavoratied.addTarget(self, action: #selector(toggleFavorite(_:)), for: .valueChanged)
        }
        
        
        
        let thread = Thread {
            sleep(2)
            DispatchQueue.main.async {
                if (restaurantId == cell.restaurantId){
                    cell.address.text = "Address: \(restaurant.address)"
                }
            }
        }
        
        thread.start()
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
        //            if (restaurantId == cell.restaurantId){
        //                cell.address.text = "Address: \(restaurant.address)"
        //            }
        //        }
        
        for subview in cell.features.subviews {
            subview.removeFromSuperview()
        }
        
        if (restaurant.features.count > 0){
            cell.featuresLabel.isHidden = false
            cell.features.isHidden = false
            
            for feature in restaurant.features {
                let featureLabel = UILabel()
                featureLabel.text = feature
                cell.features.addArrangedSubview(featureLabel)
            }
        }else{
            cell.featuresLabel.isHidden = true
            cell.features.isHidden = true
        }
        
        return cell
    }
    
    
    
    @objc func toggleFavorite(_ sender: UISwitch){
        
        let restaurantId = sender.tag
        debugPrint("set \(restaurantId) to \(sender.isOn)")
        
        if(sender.isOn){
            AddFavoratedRestaurantId(id: restaurantId)
        }else{
            RemoveFavoratedRestaurantId(id: restaurantId)
        }
        
    }
    

    func showLoading(){
        SVProgressHUD.show()
        self.tableView.isUserInteractionEnabled = false
    }
    
    func hideLoading(){
        SVProgressHUD.dismiss()
        self.tableView.isUserInteractionEnabled = true
    }
    
    func popErrDialog(errMsg: String){
        let alert = UIAlertController(title: "Not Good!", message: errMsg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {_ in
            self.refreshControl?.endRefreshing()
        }))
        
        self.present(alert, animated: true)
    }
}
