//
//  PreferencesHelper.swift
//  Plexure_challenge_ios
//
//  Created by Tony Lin on 2/05/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import Foundation

let preferences = UserDefaults.standard

let currentLevelKey = "currentLevel"

func AddFavoratedRestaurantId(id: Int){
    preferences.set(true, forKey: "\(id)")
    preferences.synchronize()
}

func RemoveFavoratedRestaurantId(id: Int){
    preferences.removeObject(forKey: "\(id)")
    preferences.synchronize()
}

func CheckFavoratedRestaurantId(id: Int) -> Bool{
    return preferences.object(forKey: "\(id)") != nil
}
