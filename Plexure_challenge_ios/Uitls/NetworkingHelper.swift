//
//  NetworkingHelper.swift
//  Plexure_challenge_ios
//
//  Created by Tony Lin on 1/05/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


func LoadData( completion: @escaping ([Restaurant], _ errMsg: String?)->Void){

    Alamofire.request(DATA_URL)
        .responseString(encoding: String.Encoding.utf8) { response in
            
            var list = [Restaurant]()
            
        guard var jsonString = response.result.value else {
            print("Invalid JSON String")
            completion(list, "Failed to retrive data from remote")
            return
        }
            
        //fix json by adding double quotes to property names
        let keys = ["id:": "id", "name:": "name", "address:": "address", "latitude:": "latitude", "longitude:": "longitude", "distance:": "distance", "featureList:": "featureList"]
        for key in keys {
            jsonString = jsonString.replacingOccurrences(of: key.key, with: "\"\(key.value)\":")
        }
   
    
            let json = JSON.init(parseJSON:jsonString)
        
        
            if let items = json.array{
                let itemCount = items.count
                
                for i in 0..<itemCount  {
                    
                    let item = items[i]
        
                    let restaurant = Restaurant(fromJSON: item)
                    
                    list.append(restaurant)
                }
            }
            
            
        
        completion(list, nil)
            
    }
    
}

class Restaurant{
    var id: Int
    var name: String
    var distance: Double
    var address: String
    var longitude: Double
    var latitude: Double
    var features: [String]

    init(fromJSON json: JSON) {
        
        id = json["id"].int ?? 0
        name = json["name"].string ?? ""
        distance = json["distance"].double ?? 0.0
        address = json["address"].string ?? ""
        longitude = json["longitude"].double ?? 0.0
        latitude = json["latitude"].double ?? 0.0
        
        features = [String]()
        if let featuresList = json["featureList"].array {
            for feature in featuresList {
                features.append(feature.string ?? "")
            }
            
        }
    }
}
