# Tony Lin's submission for to the Mobile technical challenge of Plexure!
# iOS

This app is for Tony Lin's iOS development skills assessment only.
Please click **Plexure_challenge_ios.xcworkspace** instedad of Plexure_challenge_ios.xcodeproj to open the project with the latest Xcode version

 
## The App
1. This app is a simple store app where the user can browse a list of restaurants and he/she can add to and remove from a favorites list.

3. The app runs on iOS devices/simulators of version 10.0 and newer

### Basic feature set that have been implemented

**UI:**
This app has setup 2 screens with UITabbarController. The first screen is the list of restaurants. Second screen is a favorite list that is set by the user.

*Screen of List of Restaurants:*
1. Shows the following  properties of a restaurant:  Name, Address, Distance in KM ,
 and FeatureList
2. A Pull-to-refresh mechanism is Introduceed for this list with an activity indicator.
3. Each restaurant on the list has a favorite switch. When it's switched on, the restaurant is added to the favorites list. When it's switched off, the store is removed from the favorites list.


*Screen of List of favorites:* 
1. Shows all the restaurants which are favorited. 
2. Each item on this list has a delete button. When the delete button is tapped, that store is removed from the favorites list. This is also reflected on the restaurant list.

**Networking:**
1. The app load data from the following network datasource to get the list of restaurants: https://bitbucket.org/YahiaRagaePlex/plexure-challenge/raw/449a2452c03961d5d1a094af524148cc345523db/data.json
2. The networking part is implemented with 2 3rd party frameworks, Alamofire and SwiftyJSON'

**Data handling:**

The app has a persistence layer for the app, which is utilized by the favorites list. When the app terminates for any reason and is restarted, the favorites list maintain a consistent state.

**Multithreading:**
The app performs most of the io operations with GCD(Grand Central Dispatch) to avoid heavy operations being executed in Main/UI thread

###Additional feature set that have been implemented
The app has implemented the following 4 features

* (**Data handling**) A filter feature. This feature postprocesses the received list and sorts the stores by their distance property deciding and assending. It's would be a button on top bar to filter  deciding/assending/none.

* (**Data handling**) Introduce a validation feature. This feature postprocesses the recieved list and grays out all far stores. A store is far when the the distance is more than 80 KM. Prevent users from adding far store to the favorites list.
> The value to decide if a restaurant is too far can be change in by  a constat stored in  Consts.kt.

* (**Multithreading**) Show the Address property of each store, but with a 2 seconds delay. Use the sleep() function (to simulate a heavy computing). The app should be still responsive all the time.

* (**Architecture**) Add a badge to the tab bar favorites list item. The badge should represent the number of vehicles that have been added to the favorites list since it was last viewed. When the user switches to the favorites list, the badge should disappear.

Tony Lin
